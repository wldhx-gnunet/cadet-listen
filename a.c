#include "gnunet/platform.h"
#include "gnunet/gnunet_util_lib.h"
#include "gnunet/gnunet_configuration_lib.h"
#include "gnunet/gnunet_cadet_service.h"

static struct GNUNET_CONFIGURATION_Handle *cfg;
static struct GNUNET_CADET_Handle *mh;
static struct GNUNET_CADET_Channel *ch;
static struct GNUNET_CADET_Port *lp;
static struct GNUNET_HashCode porthash;
static char *listen_port = "q";

static void
shutdown_task (void *cls)
{
	GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
			"Shutdown\n");
	if (NULL != lp)
	{
		GNUNET_CADET_close_port (lp);
		lp = NULL;
	}
	if (NULL != ch)
	{
		GNUNET_CADET_channel_destroy (ch);
		ch = NULL;
	}
	if (NULL != mh)
	{
		GNUNET_CADET_disconnect (mh);
		mh = NULL;
	}
}

static void
channel_ended (void *cls,
               const struct GNUNET_CADET_Channel *channel)
{
	GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
			"Channel ended!\n");
	GNUNET_assert (channel == ch);
	ch = NULL;
	GNUNET_SCHEDULER_shutdown ();
}

static void *
channel_incoming (void *cls,
                  struct GNUNET_CADET_Channel *channel,
                  const struct GNUNET_PeerIdentity *initiator)
{
	GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
			"Incoming connection from %s\n",
			GNUNET_i2s_full (initiator));
	GNUNET_assert (NULL == ch);
	GNUNET_assert (NULL != lp);
	GNUNET_CADET_close_port (lp);
	lp = NULL;
	ch = channel;
	return channel;
}


static void
run ()
{
	GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
			"Connecting to CADET service\n");
	mh = GNUNET_CADET_connect (cfg);
	if (NULL == mh) { GNUNET_SCHEDULER_shutdown (); return; }
	GNUNET_SCHEDULER_add_shutdown (&shutdown_task,
			NULL);
	GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
			"Opening CADET listen port\n");
	GNUNET_CRYPTO_hash (listen_port,
			strlen (listen_port),
			&porthash);
	lp = GNUNET_CADET_open_port (mh,
			&porthash,
			&channel_incoming,
			NULL,
			NULL /* window changes */,
			&channel_ended,
			NULL);
}


int main () {
	GNUNET_assert (GNUNET_OK == GNUNET_log_setup ("cadet-test", "INFO", NULL));
	cfg = GNUNET_CONFIGURATION_create ();
	GNUNET_assert (GNUNET_SYSERR != GNUNET_CONFIGURATION_load (cfg, "gnunet1.conf"));

	GNUNET_SCHEDULER_run (&run, NULL);
	//	GNUNET_SCHEDULER_shutdown();

	return 0;
}
