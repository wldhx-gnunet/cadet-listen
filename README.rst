A CADET sample
==============

Listens on a compiled-in GNUnet CADET port on start and notifies of incoming connection. Basically a stripped down version of https://gnunet.org/git/gnunet.git/tree/src/cadet/gnunet-cadet.c?id=b8da8b39b97eb830a7c03c79e5e1d8508026987c that builds out-of-tree.

Building
--------

See `build.sh`.

As of now, it has to be patched by hand - see comments in the file.

Testing
-------

See https://gnunet.org/git/gnunet-nim.git/tree/examples/start_peers.sh?id=b95e5ca8c1512c1b94d537f28ddc60f90c17b6b0.
