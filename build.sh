# As of now, this file has to be patched by hand in two places:
# 1. Make sure path in `-p` points to your gnunet repo
# 2. In --run, linker flags after $NIX_CFLAGS_COMPILE ought to be
#    taken from `nix-shell -p '(import ~/gnunet { }).dev' --run "export | grep NIX_LDFLAGS"
#    (just replace the -L entries from ^ in `--run`)
nix-shell \
	-p '(import ~/gnunet { }).dev' \
	--run '$NIX_CC/bin/gcc -g -Wall $NIX_CFLAGS_COMPILE -L/nix/store/rm0cnbmn5wg6wl7k467x6z42ddp5zpk9-libgcrypt-1.8.3/lib -L/nix/store/2yk4zyrrcv9nb5h73ls7qa5qp4z3d5a1-gnunet-dev/lib -L/nix/store/rm0cnbmn5wg6wl7k467x6z42ddp5zpk9-libgcrypt-1.8.3/lib -lgnunetcadet -lgnunetutil a.c'
